package application.services.implementations;

import application.objects.Role;
import application.objects.TaskList;
import application.objects.User;
import application.repositories.RoleRepository;
import application.repositories.UserRepository;
import application.services.interfaces.TaskListService;
import application.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TaskListService taskListService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void saveUser(User user)     {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1);
        Role userRole = roleRepository.findByRole("USER");
        user.setRoles(new HashSet<>(Collections.singletonList(userRole)));
        userRepository.save(user);

        TaskList todo = new TaskList();
        todo.setName("TODO:");
        todo.setShowOrder(1);
        todo.setDate(new Timestamp(new Date().getTime()));
        todo.setUser(user);

        TaskList inProgress = new TaskList();
        inProgress.setName("In Progress:");
        inProgress.setShowOrder(2);
        inProgress.setDate(new Timestamp(new Date().getTime()));
        inProgress.setUser(user);

        TaskList done = new TaskList();
        done.setName("Done:");
        done.setShowOrder(3);
        done.setDate(new Timestamp(new Date().getTime()));
        done.setUser(user);

        LinkedList<TaskList> lists = new LinkedList<>();
        lists.add(todo);
        lists.add(inProgress);
        lists.add(done);
        taskListService.saveAll(lists);
    }
}
