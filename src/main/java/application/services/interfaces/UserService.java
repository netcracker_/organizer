package application.services.interfaces;

import application.objects.User;

public interface UserService {
    User findUserByEmail(String email);
    void saveUser(User user);
}
