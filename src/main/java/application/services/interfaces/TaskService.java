package application.services.interfaces;

import application.objects.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface TaskService extends CrudRepository<Task, Integer> {
}
