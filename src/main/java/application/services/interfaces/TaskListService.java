package application.services.interfaces;

import application.objects.TaskList;
import application.objects.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface TaskListService extends CrudRepository<TaskList, Integer> {

    Iterable<TaskList> findAllByOrderByDateAsc();

    Iterable<TaskList> findAllByUserOrderByShowOrderAsc(User user);

    @Query(value = "SELECT MAX(show_order) FROM task_list WHERE user_id = :user", nativeQuery = true)
    Integer findMaxOrder(@Param("user") String user);
}
