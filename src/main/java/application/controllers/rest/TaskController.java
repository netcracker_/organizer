package application.controllers.rest;

import application.objects.Task;
import application.services.interfaces.TaskListService;
import application.services.interfaces.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskListService taskListService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Task addTask(@RequestParam(value = "name") String name,
                        @RequestParam(value = "listid") Integer listID){
        Task task = new Task();
        task.setName(name);
        task.setDescription("");
        task.setDate(new Timestamp(new Date().getTime()));
        task.setList(taskListService.findById(listID).get());
        taskService.save(task);
        return task;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Iterable<Task> getAllTasks(){
        return taskService.findAll();
    }

    @RequestMapping(value = "/id/{ID}", method = RequestMethod.GET)
    public Optional getTaskById(@PathVariable Integer ID){
        return taskService.findById(ID);
    }

    @RequestMapping(value = "/delete/{ID}", method = RequestMethod.DELETE)
    public void deleteTaskById(@PathVariable Integer ID){
        taskService.deleteById(ID);
    }

    @RequestMapping(value = "/update/{ID}", method = RequestMethod.PUT)
    public Task updateTaskByID(@PathVariable(value = "ID") Integer ID,
                               @RequestParam Map<String, String> params){
        Task task = taskService.findById(ID).get();
        if (params.containsKey("listid")){
            task.setList(taskListService.findById(Integer.valueOf(params.get("listid"))).get());
            task.setDate(new Timestamp(new Date().getTime()));
        }
        if (params.containsKey("name")){
            task.setName(params.get("name"));
        }
        if (params.containsKey("description")){
            task.setDescription(params.get("description"));
        }
        taskService.save(task);
        return task;
    }
}
