package application.controllers.rest;

import application.objects.TaskList;
import application.objects.User;
import application.services.interfaces.TaskListService;
import application.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.*;

@RestController
@RequestMapping("/list")
public class TaskListController {

    @Autowired
    private TaskListService taskListService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Iterable<TaskList> getAllListsByUser() {
        return taskListService.
                findAllByUserOrderByShowOrderAsc(userService.findUserByEmail(SecurityContextHolder.
                                getContext().
                                getAuthentication().
                                getName()));
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public TaskList addList(@RequestParam(value = "name") String name){
        User user = userService.findUserByEmail(SecurityContextHolder.
                getContext().
                getAuthentication().
                getName());
        TaskList taskList = new TaskList();

        taskList.setName(name);
        taskList.setDate(new Timestamp(new Date().getTime()));
        taskList.setShowOrder(taskListService.findMaxOrder(Integer.toString(user.getId())) == null ?
                                 1 : taskListService.findMaxOrder(Integer.toString(user.getId())) + 1);
        taskList.setUser(user);
        taskListService.save(taskList);
        return taskList;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteListById(@PathVariable Integer id){
        taskListService.deleteById(id);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public TaskList getListById(@PathVariable Integer id){
        return taskListService.findById(id).get();
    }

    @RequestMapping(value = "/replace", method = RequestMethod.POST)
    public void replaceLists(@RequestParam(value = "left") Integer leftID,
                             @RequestParam(value = "right") Integer rightID){
        TaskList leftList = taskListService.findById(leftID).get();
        TaskList rightList = taskListService.findById(rightID).get();
        Integer temp = leftList.getShowOrder();
        leftList.setShowOrder(rightList.getShowOrder());
        rightList.setShowOrder(temp);
        taskListService.save(leftList);
        taskListService.save(rightList);
    }

    @RequestMapping(value = "/update/{ID}", method = RequestMethod.PUT)
    public TaskList updateListById(@PathVariable(value = "ID") Integer ID,
                                   @RequestParam(value = "name") String name){
        TaskList taskList = taskListService.findById(ID).get();
        taskList.setName(name);
        taskListService.save(taskList);
        return taskList;
    }

}
