package application.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@ResponseBody
public class PageController {
    @GetMapping(value = "/index")
    public ModelAndView getIndex(){
        return new ModelAndView("index");
    }
}
