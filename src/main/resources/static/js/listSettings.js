window.addEventListener("mousedown", function onMouseDown(event) {
    let e = event || window.event;
    if ((!e.target.closest("#list_settings")) &&
        (document.getElementById("list_settings").style.display !== "none")){
        //document.getElementById("list_settings").style.display = "none";
        if (!e.target.closest(".task_list_tools"))
            $("#list_settings").fadeOut(200);
        else $("#list_settings").fadeOut(1);
    }
});

