function hideTaskDeatils(){
    $('#modal_form')
        .animate({opacity: 0, top: '45%'}, 200,
            function(){
                $(this).css('display', 'none');
                $('#overlay').fadeOut(200);
            }
        );
    $('#task_description').unbind('keydown');
    $('#task_description').unbind('focus');
    $('#task_description').unbind('blur');
    $('#task_basket').unbind('click');
    $("#task_description_button").unbind('click');
}

$(document).ready(function() {
    $('#modal_close, #overlay').click(hideTaskDeatils);
});

