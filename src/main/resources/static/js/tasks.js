var content = new Vue({
    el: "#content",
    data: {
        lists: [],
        input_value_name: "",
        input_value_description: ""
    },
    created() {
        this.fetchLists();
    },
    methods: {
        fetchLists() {
            axios.get("/list/all").then(function (response) {
                this.lists = response.data;
            }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
        },
        addList(event) {
            event = event || window.event;
            if (event.keyCode === 13 || event.type === 'click') {
                let name = document.getElementById("list_input");
                if (name.value === "") {
                    alert("Enter list name");
                    return;
                }

                const params = new URLSearchParams();
                params.append("name", name.value);
                axios.post("list/add", params).then(function (response) {
                    this.lists.push(response.data);
                }.bind(this))
                    .catch(function (error) {
                        console.log(error);
                    }).finally(function () {
                    name.blur();
                });
            }
        },
        deleteList(id) {
            document.getElementById("list_settings").style.display = "none";
            axios.delete("list/delete/" + id).then(function (response) {
                this.lists = this.lists.filter(function (list) {
                    return list.listId !== id;
                });
            }.bind(this))
                .catch(function (error) {
                    console.log(error);
                })
        },
        addTask(listID, event) {
            event = event || window.event;
            if (event.keyCode === 13 || event.type === 'click') {
                let name = document.getElementById("task_input_" + listID);
                if (name.value === "") {
                    alert("Enter task name");
                    return;
                }

                const params = new URLSearchParams();
                params.append("name", name.value);
                params.append("listid", listID);
                axios.post("task/add", params).then(function (response) {
                    this.lists.forEach(function (list) {
                        if (list.listId === listID) {
                            if (!list.tasks) list.tasks = [];
                            list.tasks.push(response.data);
                        }
                    });
                    // $.each($('.task_list_content'), function () {
                    //     this.scrollTop = 10000;
                    // });
                }.bind(this))
                    .catch(function (error) {
                        console.log(error);
                    }).finally(function () {
                    name.blur();
                });
            }
        },
        deleteTask(listID, taskID) {
            axios.delete("/task/delete/" + taskID).then(function (response) {
                let list = this.lists.find(function (list) {
                    return list.listId === listID;
                });
                list.tasks = list.tasks.filter(function (task) {
                    return task.id !== taskID;
                });
            }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
        },
        moveTask(taskID, oldListID, newListID){
            const params = new URLSearchParams();
            params.append("listid", newListID);
            axios.put("task/update/" + taskID, params)
                .then(function (response) {
                    var lists = this.lists;

                    var oldList =  lists.find(function (list) {
                        return list.listId === oldListID;
                    });
                    oldList.tasks = oldList.tasks.filter(function (task) {
                        return task.id !== taskID;
                    });

                    var newList = lists.find(function (list) {
                        return list.listId === newListID;
                    });
                    if (!newList.tasks) newList.tasks = [];
                    newList.tasks.push(response.data);

                    this.lists = lists;

                }.bind(this))
                .catch(function (error) {
                console.log(error);
            });
        },
        showListSettings(listID){
            var toolsButton = document.getElementById("task_list_tools_" + listID);
            var coords = {
                x: toolsButton.getBoundingClientRect().left,
                y: toolsButton.getBoundingClientRect().top
            };
            var settingsBlock = document.getElementById("list_settings");
            settingsBlock.style.left = document.body.scrollLeft + coords.x - 90 + "px";
            settingsBlock.style.top = coords.y + "px";
            var onlyListFlag = this.lists.length === 1;
            var list = this.lists.find(function (list) {
                return list.listId === listID;
            });
            var leftExtremeListFlag = this.lists.indexOf(list) === 0;
            var rightExtremeListFlag = this.lists.indexOf(list) === this.lists.length - 1;
            $('#list_settings_left_point').css('display', 'block');
            $('#list_settings_right_point').css('display', 'block');

            if (!onlyListFlag){
                if (leftExtremeListFlag){
                    $('#list_settings_left_point').css('display', 'none');
                }
                else if (rightExtremeListFlag){
                    $('#list_settings_right_point').css('display', 'none');
                }
            } else {
                $('#list_settings_left_point').css('display', 'none');
                $('#list_settings_right_point').css('display', 'none');
            }

            $("#list_settings").fadeIn(200);

            document.getElementById("list_settings_basket").onclick = () => (this.deleteList(listID));
            document.getElementById("list_settings_left_point").onclick = () => (this.moveLeftList(listID));
            document.getElementById("list_settings_right_point").onclick = () => (this.moveRightList(listID));
            document.getElementById("list_settings_close").onclick = () => (
                $("#list_settings").fadeOut(200)
            );

        },
        moveLeftList(listID){
            document.getElementById("list_settings").style.display = "none";
            var rightListElem = document.getElementById("list_" + listID);
            var leftListElem = rightListElem.previousElementSibling;
            if (leftListElem){
                this.moveLists(Number(leftListElem.id.split("list_")[1]),
                               Number(rightListElem.id.split("list_")[1]));
            }
        },
        moveRightList(listID){
            document.getElementById("list_settings").style.display = "none";
            var leftListElem = document.getElementById("list_" + listID);
            var rightListElem = leftListElem.nextElementSibling;
            if (!rightListElem.classList.contains("list_input")){
                this.moveLists(Number(leftListElem.id.split("list_")[1]),
                               Number(rightListElem.id.split("list_")[1]));
            }
        },
        moveLists(leftListId, rightListId){
            const params = new URLSearchParams();
            params.append("left", leftListId);
            params.append("right", rightListId);
            axios.post("list/replace", params)
                .then(function (response) {
                    leftList = this.lists.find(function (list) {
                        return list.listId === leftListId;
                    });
                    leftIndex = this.lists.indexOf(leftList);
                    rightList = this.lists.find(function (list) {
                        return list.listId === rightListId;
                    });
                    rightIndex = this.lists.indexOf(rightList);

                    this.lists.splice(leftIndex, 1, rightList);
                    this.lists.splice(rightIndex, 1, leftList);
                }.bind(this)).catch(function (error) {
                console.log(error);
            });
        },
        updateListNameStart(listId, event){
            var field = document.getElementById("list_" + listId + "_header");
            this.setInputToEnd(field);

        },
        updateListNameEnd(listId, event){
            event = event || window.event;
            if(event.keyCode === 13) event.target.blur();
            else if (event.type === 'blur') {
                var header = document.getElementById("list_" + listId + "_header");
                var newName = header.textContent;
                if (newName === "" || newName === this.input_value_name){
                    header.textContent = this.input_value_name;
                    this.input_value_name = "";
                    return;
                }

                const params = new URLSearchParams();
                params.append("name", newName);
                axios.put("/list/update/" + listId, params)
                    .then(function (response) {
                        var list = this.lists.find(function (list) {
                            return list.listId === listId;
                        });
                        list.name = newName;
                    }.bind(this))
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        },
        updateTaskNameBegin(taskId, listId){
            document.getElementById("task_" + taskId + "in_list_" + listId + "_header").style.display = 'none';
            var stub = document.getElementById("task_" + taskId + "in_list_" + listId + "_stub");
            stub.style.display = 'block';
            this.setInputToEnd(stub);
        },
        updateTaskNameEnd(taskId, listId, event){
            event = event || window.event;
            if(event.keyCode === 13) event.target.blur();
            else if (event.type === 'blur'){
                var header = document.getElementById("task_" + taskId + "in_list_" + listId + "_header");
                var stub = document.getElementById("task_" + taskId + "in_list_" + listId + "_stub");
                header.style.display = 'block';
                stub.style.display = 'none';
                var newName = stub.textContent;
                if (newName === "" || newName === this.input_value_name){
                    stub.textContent = this.input_value_name;
                    this.input_value_name = "";
                    return;
                }

                const params = new URLSearchParams();
                params.append("name", newName);
                axios.put("/task/update/" + taskId, params)
                    .then(function (response) {
                        var list = this.lists.find(function (list) {
                            return list.listId === listId;
                        });
                        var task = list.tasks.find(function (task) {
                            return task.id === taskId;
                        });
                        task.name = newName;

                    }.bind(this))
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        },
        setInputToEnd(elem){
            if (typeof window.getSelection !== "undefined"
                && typeof document.createRange !== "undefined") {
                var range = document.createRange();
                range.selectNodeContents(elem);
                range.collapse(false);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (typeof document.body.createTextRange !== "undefined") {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(elem);
                textRange.collapse(false);
                textRange.select();
            }
        },
        saveInputValueName(event){
            event = event || window.event;
            this.input_value_name = event.target.textContent;
        },
        onClickInputTask(listId){
            var button = document.getElementById("task_input_button_" + listId);
            button.style.display = "block";
        },
        onBlurInputTask(listId){
            var button = document.getElementById("task_input_button_" + listId);
            var field = document.getElementById("task_input_" + listId);
            setTimeout(function () {
                button.style.display = "none";
                field.value = "";
            }, 100);

        },
        onClickInputList(){
            var button = document.getElementById("list_input_button");
            button.style.display = "block";
        },
        onBlurInputList(){
            var field = document.getElementById("list_input");
            var button = document.getElementById("list_input_button");
            setTimeout(function () {
                button.style.display = "none";
                field.value = "";
            }, 100);
        },
        showTaskTools(listID, taskID){
            var taskDescription = $('#task_description');
            taskDescription.val(content.lists.find(function (list) {
                return list.listId === listID;
            }).tasks.find(function (task) {
                return task.id === taskID;
            }).description);

            //event.preventDefault();
            $('#overlay').fadeIn(200,
                function(){
                    $('#modal_form')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });

            taskDescription.focus(function () {
                $("#task_description_button").css('display', 'block');
            });

            taskDescription.blur(function () {
                setTimeout(function () {
                    taskDescription.val(content.lists.find(function (list) {
                        return list.listId === listID;
                    }).tasks.find(function (task) {
                        return task.id === taskID;
                    }).description);
                    $("#task_description_button").css('display', 'none');
                }, 100);
            });

            taskDescription.keydown(function (e) {
                if (e.keyCode === 13){
                    content.updateTaskDescription(taskID, listID);
                }
            });

            $("#task_description_button").click(function () {
                $(this).css('display', 'none');
                content.updateTaskDescription(taskID, listID);
            });

            $('#task_basket').click(function () {
                content.deleteTask(listID, taskID);
                hideTaskDeatils();
            });
        },
        updateTaskDescription(taskID, listID){
            var taskDescription = $('#task_description');
            var value = taskDescription.val();
            content.lists.find(function (list) {
                return list.listId === listID;
            }).tasks.find(function (task) {
                return task.id === taskID;
            }).description = value;

            taskDescription.blur();

            const params = new URLSearchParams();
            params.append("description", value);
            axios.put("/task/update/" + taskID, params)
                .then(function (response) {}).catch(function (error) {
                console.log(error);
            });
        },
    },
});